package modeling;
import Jama.Matrix; 
import java.util.Scanner; 

public class Matrice{

public static void main(String [] args) { 
	Scanner scan=new Scanner(System.in); 
	System.out.println("Введите длину массива y"); // Вводим длину массива у 
	
	int n=scan.nextInt(); 
	double[] y=my(n); // Вводим массив у 
	double[][] x=mx(n); // Вводим массив х 
	double[] z=test(x,y); // Высчитываем коэффициенты 
	System.out.println(" "); 
	
	C b = new C(new B()); 
	double[] yy=b.cl(x,z); 
	double YY=metod(yy); // Высчитывает прогноз Y2 
	double YYY=metod2(z); // Высчитывает прогноз Y1 
	
	System.out.println("Оценка точности первого прогноза"); 
	K p= new K(new D()); 
	double T1=p.cl(x, z, YYY); // Оценка точности первого прогноза 
	System.out.println("T1="+T1); 
	System.out.println("Оценка точности второго прогноза"); 
	
	K r= new K(new D()); 
	double T2=r.cl(x, z, YY); // Оценка точности второго прогноза 
	System.out.println("T2="+T2); 
} 

public static double[] my(int n){ 
	Scanner scan=new Scanner(System.in); 
	double y[]=new double[n]; 
	System.out.println("Введите элементы массива у"); // Вводим элементы массива у 
	
	for (int i=0; i< n;i++) { 
		y[i]=scan.nextInt();} 
	return y; } 


public static double[][] mx(int n){ 
	Scanner scan=new Scanner(System.in); 
	double x[][]=new double[4][n]; 
	
	System.out.println("Введите элементы массива x1"); // Вводим элементы массива х1 
	for (int i=0; i<n;i++) { 
		
		x[1][i]=scan.nextInt();} 
	System.out.println("Введите элементы массива x2"); // Вводим элементы массива х2 
	
	for (int i=0; i<n;i++) { 
		x[2][i]=scan.nextInt();} 
	System.out.println("Введите элементы массива x3"); // Вводим элементы массива х3 

	for (int i=0; i<n;i++) { 
		x[3][i]=scan.nextInt();} 
	
	for(int i=0; i<4; i++) { // Присваиваем факторам х1 и х3 нужные значения, исходя из формулы 

		x[2][i]=Math.log(x[2][i]*x[2][i]); } 
	
	for(int i=0; i<4; i++) {
		x[1][i]=Math.cos(x[1][i]);}

	for(int i=0; i<4; i++) { 

		x[3][i]=Math.log(x[3][i]); } 
	
	for (int i=0; i<n; i++) { 
		x[0][i]=1; } 
return x; } 


public static double[] test(double[][]ff, double[]yy){ 
	
	double z[]=new double[4]; 
	Matrix A1=new Matrix(ff); 
	A1.print(3,4); 
	Matrix B1=A1.transpose(); 
	Matrix F1=A1.times(B1); 
	Matrix F4=F1.inverse(); 
	Matrix F2=F4.times(A1); 
	Matrix C=new Matrix(yy,4); 
	Matrix F3=F2.times(C); 
	z=F3.getColumnPackedCopy(); 
	
	for (int i = 0; i < 4; i++) { 
		System.out.println("z["+i+"]="+z[i]); // Считаем коэффициенты z0 z1 z2 z3 
} 
	return z; } 

public static double metod2(double [] z) { 
	
	Scanner scan=new Scanner(System.in); 
	double xx[]=new double[3]; 
	System.out.println("Введите элементы массива xx"); 
	
	// Вводим факторы х1 х2 х3, чтобы посчитать прогноз у=z[0]+z[1]*Math.cos(xx[0])+z[2]*Math.log(xx[1]*xx[1])+z[3]*Math.log(xx[2]) 
	
	for (int i=0; i< 3;i++) { 
		xx[i]=scan.nextInt();} 
	
	double YYY=z[0]+z[1]*Math.cos(xx[0])+z[2]*Math.log(xx[1]*xx[1])+z[3]*Math.log(xx[2]); // Высчитываем прогноз у 
	
	System.out.println("Прогноз Y1"); 
	System.out.println("Y1="+YYY); 
	return YYY; 
} 

public static double metod(double[] yy) { 
	double t=(1+2+3+4)/4; // Среднее t со штрихом 
	double y1=0; 
	
	for (int i=0; i<4; i++) { 
		y1=yy[i];} 
	y1=y1/4; // Cреднее у со штрихом 
	double ty=0; 

	for (int i=0; i<4; i++) { 
		ty=(i+1)*yy[i];} 
	ty=ty/4; // Высчитываем t*y со штрихом 
	
	double tt=0; 

	for (int i=0; i<4; i++) { 
		tt=(i+1)*(i+1)/4; } 
	tt=tt/4; // Высчитываем t*t со штрихом 

	double b=(ty-t*y1)/(tt-t*t); // Высчитываем b 
	double a=y1-b*t; // Высчитываем а 
	double T=4; // Задаем T, во время которого будем высчитывать прогноз Y2 
	double Y=a+b*T; // Высчитываем прогноз Y2 
	System.out.println("Прогноз Y2"); 
	System.out.println("Y2="+Y); 
	return Y; }} 

class B implements E { 
	public double[] ex(double[][]x, double[]z) { 
		double[] y=new double[4]; 
		for (int i=0; i<4; i++) { 
			y[i]=z[0]+z[1]*x[1][i]+z[2]*x[2][i]+z[3]*x[3][i]; } // Высчитываем у0, у1, у2, у3, подставляя коэффициенты z 

		for (int i=0; i<4; i++) { 
			System.out.println("y["+i+"]="+y[i]); } 
		return y; } 
} 

interface E { 
	double[] ex(double[][]x, double[]z); } 

class C { 
	double[] f; 
	E handler; 

	C(E action) { 
		this.handler = action; 
} 
	
	public double[] cl(double[][]x, double[]z) { 
		return f = handler.ex(x,z); 
}} 

class D implements E{ 

	public double[] ex(double[][] x, double[] z) { 
	double[] y=new double[4]; 
		for (int i=0; i<4; i++) { 
			y[i]=z[0]+z[1]*x[1][i]+z[2]*x[2][i]+z[3]*x[3][i]; } 
		
	double t=(1+2+3+4)/4; 
	double y1=0; 
		for (int i=0; i<4; i++) { 
			y1=y[i];} 
	y1=y1/4; 
	
	double ty=0; 
	for (int i=0; i<4; i++) { 
		ty=(i+1)*y[i];} 
	
	ty=ty/4; 

	double tt=0; 
	for (int i=0; i<4; i++) { 
		tt=(i+1)*(i+1)/4; } 
	tt=tt/4; 
	double b=(ty-t*y1)/(tt-t*t); 
	double a=y1-b*t; 
	double T=4; 
	double[] Y=new double[1]; 
	Y[0]=a+b*T; 
	return Y; 
}}

class K { 
	double[] f; 
	E handler; 

	K(E action) { 
		this.handler = action; 
} 
	
public double cl(double[][]x, double[]z,double Y) { 

	handler.ex(x,z); 
	
	double T2=Math.abs((Y-5)/5); // Высчитываем оценку точности прогноза Y2 
	// 5 это у=m+1,переменная, значение которой мы задаем сами 
return T2; 
}}
