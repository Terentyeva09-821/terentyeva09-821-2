import java.io.BufferedReader; 
import java.io.File; 
import java.io.FileInputStream; 
import java.io.FileNotFoundException; 
import java.io.IOException; 
import java.io.InputStreamReader; 
import java.io.UnsupportedEncodingException; 

public class Interval { 


// Основные переменные 
private static String fname = "student.csv"; // файл с данными 
private static float k[][] = new float[3][3]; 
private static String s[]=new String[3]; 


// Разделитель данных в файле 
private static final String separator = ";"; 

// Основная программа 
public static void main(String[] args) { 

// Считаем данные из файла 
ReadCSV(); 
double sb; 
for (int i=0; i<3; i++){ 
sb=0; 
for (int j=0; j<3; j++){ 
sb=(double)(k[i][j]+sb);} 
sb=sb/3; 

System.out.println(s[i]+" "+sb); } 
} 

// Прочитать данные из файла в массив 
public static void ReadCSV() { 
File file = new File(fname); 
try(BufferedReader br= new BufferedReader(new InputStreamReader(new FileInputStream(file), "Windows-1251"));) { 
String line = ""; 
int i=0; 
// Считываем файл построчно \n 
line = br.readLine(); 
while ((line = br.readLine()) != null) { 
String [] elements = line.split(separator); 
s[i]=elements[0]; 
k[i][0]=Float.parseFloat(elements[1]); 
k[i][1]=Float.parseFloat(elements[2]); 
k[i][2]=Float.parseFloat(elements[3]); 
i++; 
} 
} catch (UnsupportedEncodingException e) { 
e.printStackTrace(); 
} catch (FileNotFoundException e) { 
e.printStackTrace(); 
} catch (IOException e) { 
e.printStackTrace(); 
} 
}}